{
    "_v": "17.4",
    "clients": [
        {
            "allowed_origins": [
                "http://blog.yourwordpressaddress.com",
                "https://blog.yourwordpressaddress.com"
            ],
            "client_id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            "resources": [
                {
                    "methods": [
                        "get",
                        "patch",
                        "put"
                    ],
                    "read_attributes": "(**)",
                    "resource_id": "/libraries/*/content/*",
                    "write_attributes": "(**)"
                },
                {
                    "methods": [
                        "get"
                    ],
                    "read_attributes": "(**)",
                    "resource_id": "/libraries/*/content/*/folders",
                    "write_attributes": "(**)"
                },
                {
                    "methods": [
                        "put",
                        "get"
                    ],
                    "read_attributes": "(**)",
                    "resource_id": "/libraries/*/folder_assignments/*/*",
                    "write_attributes": "(**)"
                },
                {
                    "methods": [
                        "get"
                    ],
                    "read_attributes": "(**)",
                    "resource_id": "/libraries/*/folders/*/content",
                    "write_attributes": "(**)"
                },
                {
                    "methods": [
                        "get"
                    ],
                    "read_attributes": "(**)",
                    "resource_id": "/libraries/*/folders/*/sub_folders",
                    "write_attributes": "(**)"
                }
            ],
            "response_headers": {
                "P3P": "CP=\"NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM\"",
                "x-foo": "bar"
            }
        }
    ]
}