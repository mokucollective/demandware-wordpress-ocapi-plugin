<?php

class dwAdminAssets {
    
    
    public function __construct() {           	
        add_action('admin_enqueue_scripts', array($this, 'addCSS'));
        add_action( 'admin_enqueue_scripts', array($this,'enqueue_admin_js') );
    }
    
    public function addCSS () {    	
        echo '<link rel="stylesheet" href="'.DWO_ADMIN_WEB_URI.'/assets/admin.css" type="text/css" media="all" />';     
    }
    
    public function enqueue_admin_js(){
    	wp_enqueue_script('dwcustom', plugins_url('assets/custom.js',__FILE__),array('jquery'),  '', true  );
    	wp_localize_script('dwcustom', 'MyDwAjax', array('ajaxurl'   => admin_url('admin-ajax.php')));
    }
        
}

$dwAdminAssets = new dwAdminAssets();
