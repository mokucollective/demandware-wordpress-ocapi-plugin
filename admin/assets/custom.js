jQuery(document).ready(function() {
	// publish button validation
	jQuery('.post-type-dw-servers #publish').click(function(){
		$title_value = jQuery.trim(jQuery('#title').val());
		if($title_value == 0 && $title_value != " "){
			jQuery('#title').attr("required", true);
			jQuery('.spinner').attr("visibility","hidden");
			jQuery('#title').focus();
			//return false;
		}
	});
	// draft button validation
	jQuery('.post-type-dw-servers #save-post').click(function(){
		$title_value = jQuery.trim(jQuery('#title').val());
		if($title_value == 0 && $title_value != " "){
			jQuery('#title').attr("required", true);
			jQuery('.spinner').attr("visibility","hidden");
			jQuery('#title').focus();
			//return false;
		}
	});
	
	jQuery('.push_spinner').css('visibility', 'hidden');
	jQuery(".btn-push").click( function() {
		  jQuery('.previewLink').html('');
		  postID = jQuery(this).data("postid");
		  serverID= jQuery("#push_to_server").val();	
		  if(serverID != "")
		  {	
			  jQuery.ajax({
		    	  type: 'POST',
		          url: MyDwAjax.ajaxurl,
		          data : {
		        	  	action  : 'get_post_data',
						postID  : postID,
						serverID: serverID
		          },
		          beforeSend: function() {
		        	  jQuery('.push_spinner').css('visibility', 'visible');
				  },
				  success: function(data, textStatus, XMLHttpRequest) {
					  var postdata = jQuery.parseJSON(data);
					  
					  if(postdata.publish_status == false)
					  {
						  alert("Publish Post First!");
					  }
					  else
					  {
						  if(postdata.push_status == true)
						  {
							  alert("Post Push Successfully.");
							  jQuery('.previewLink').html('<a href="'+postdata.previewLink+'" target="_blank">Preview Post on Demandware</a>');
						  }
							  
						  else
							  alert("Error in Push Post.\n" + postdata.push_error);
					  }
				  },
		          complete: function() {
		        	  jQuery('.push_spinner').css('visibility', 'hidden');
		          },
			  }); 
		  }
		  else
		  {
			  alert("Please Select Server First!");
			  return false;
		  }
	  });
	jQuery(".btn-push-all").click( function() {			
		  serverID = jQuery(this).data("postid");
		  jQuery.ajax({
	    	  type: 'POST',
	          url: MyDwAjax.ajaxurl,
	          data : {
	        	  	action: 'push_all',
	        	  	serverID: serverID
	          },
	          beforeSend: function() {
	        	  jQuery('.push_spinner').css('visibility', 'visible');
			  },
			  success: function(data, textStatus, XMLHttpRequest) {
				  var postdata = jQuery.parseJSON(data);				  
				  if(postdata.publish_status == false)
				  {
					  alert("Publish Post First!");
				  }
				  else
				  {
					  if(postdata.server_status == false)
					  {
						  alert("Server is Disabled!");
					  }
					  else
					  {
						  if(postdata.push_status == true)
							  alert("All Posts Push Successfully.");
						  else
							  alert("Error in Push Post.");
					  }
				  }
			  },
	          complete: function() {
	        	  jQuery('.push_spinner').css('visibility', 'hidden');
	          },
		 });	  
	});		
});