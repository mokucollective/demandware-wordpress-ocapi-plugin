<?php

add_meta_box ( 'dw-post-metaboxes', 'Push to Salesforce', 'dw_post_metaboxes_cb', 'post', 'side', 'default' );

function dw_post_metaboxes_cb($post) {
	wp_nonce_field ( plugin_basename ( __FILE__ ), 'dw_postmeta_nonce' );
	$values = get_post_custom ( $post->ID );
	$serverPosts = get_posts(array('post_type'=>'dw-servers','numberposts'=>-1));
	?>
	<div class="misc-pub-section misc-pub-post-status">
		Push Post to 
		<select name="push_to_server" id="push_to_server">
			<option value="">Select Server</option>
			<?php
			foreach( $serverPosts as $serverPost )
			{
				$serverStatus = get_post_meta($serverPost->ID,"dw_server_status",true);
				if($serverStatus == "Enabled")
					echo '<option value="' . $serverPost->ID. '">' . esc_html( $serverPost->post_title ) . '</option>';
			}
			?>
		</select>
	</div>
	<div class="misc-pub-section misc-pub-post-status">
		<span class="push_spinner spinner">&nbsp;</span>
		<input type="button" id="push" name="push" class="button button-primary button-large btn-push" value="Push" data-postid="<?php echo $post->ID;?>" />
		<a href="<?php echo admin_url("edit.php?post_type=dw-servers");?>" class="dashicons dashicons-admin-generic settings-servers-icon"></a>
	</div>
	<div class='previewLink'></div>
<?php
}

add_meta_box ( 'dw-post-metaboxes', 'Push all posts to Salesforce', 'dw_server_metaboxes_cb', 'dw-servers', 'side', 'default' );

function dw_server_metaboxes_cb($post) {
	wp_nonce_field ( plugin_basename ( __FILE__ ), 'dw_postmeta_nonce' );
	$values = get_post_custom ( $post->ID );
	?>
	<div class="misc-pub-section misc-pub-post-status">
		<span class="push_spinner spinner">&nbsp;</span>
		<input type="button" id="push" name="push" class="button button-primary button-large btn-push-all" value="Push" data-postid="<?php echo $post->ID;?>" />
	</div>	
<?php
}