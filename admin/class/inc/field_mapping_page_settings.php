<?php
echo "<h1>Mapping Settings</h1>";
$dw_metas = $this->dwMeta;
$dw_custom_meta = json_decode ( get_option ( $this->dw_custom_meta_settings_key ) );
if (isset ( $_POST ['update_mapping_meta'] )) {
	
	foreach ( $dw_metas as $key => $value ) {
		if (isset ( $_POST [$key] )) {
			$meta_elements [$key] = esc_attr ( $_POST [$key] );
		}
	}
	update_option ( $this->dw_meta_settings_key, json_encode ( $meta_elements ) );
	if ((isset ( $_POST ['dw_custom'] ) && isset ( $_POST ['wp_custom'] )) && (! empty ( $_POST ['wp_custom'] [0] ) && ! empty ( $_POST ['dw_custom'] [0] ))) {
		$dw_custom = $_POST ['dw_custom'];
		$wp_custom = $_POST ['wp_custom'];
		$meta_custom_elements = array_combine ( $dw_custom, $wp_custom );
		update_option ( $this->dw_custom_meta_settings_key, json_encode ( $meta_custom_elements ) );
	} else
		update_option ( $this->dw_custom_meta_settings_key, '' );
	echo '<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"> 
<p><strong>Field mapping settings saved.</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
}

?>

<form method="POST" action="">
	<table class="form-table">
		<tbody>
			<tr>
				<th colspan="3">Default Mapping Settings <br /> <br /> Map the
					default Salesforce Commerce Cloud fields with any WordPress fields to your Content
					Assests
				</th>
			</tr>	
			<?php
			$preserveArr = json_decode ( get_option ( $this->dw_meta_settings_key ) );
			$totalRow = count ( ( array ) $preserveArr ) + 2;
			$totalCustomRow = count ( $dw_custom_meta );
			echo '<tr><td style="padding-left:0px; margin-left:0px;"><strong>Salesforce Field</strong></td><td><strong>WordPress Field</strong></td><td rowspan="' . $totalRow . '" style="vertical-align:top;">				
			<table style="border-style: dashed;">
				<tbody>
					<tr><td><strong>Field</strong></td><td><strong>Code</strong></td></tr>
					<tr><td>Post Id:</td><td>post_id</td></tr>
					<tr><td>Post Title/Name:</td><td>post_title</td></tr>
					<tr><td>Post Description:</td><td>post_description</td></tr>
					<tr><td>Post Status:</td><td>post_status</td></tr>
					<tr><td>Post Tags:</td><td>post_tags</td></tr>
					<tr><td>Custom Field:</td><td>field:[field_name]</td></tr>											
				</tbody>						
			</table>	
			</td></tr>';
			?>								
			<?php
			
			foreach ( $dw_metas as $key => $value ) {
				$tooltip = ((isset ( $value ['tooltip'] ) && $value ['tooltip'] != "") ? "<a class=tooltips href=#><span>" . $value ['tooltip'] . "</span></a>" : "");
				echo '<tr><th scope="row"><label for="' . $key . '">' . $key . ' :</label></th> <td><input type="text" name="' . $key . '" id="' . $key . '" placeholder="Specify field name to be map with: ' . $key . '" class="regular-text" value="' . (isset ( $preserveArr->$key ) ? $preserveArr->$key : "") . '">' . $tooltip . '</td></tr>';
			}
			?>
			<tr>
				<td colspan='2'><hr /></td>
			</tr>
			<tr>
				<th colspan="2">Custom Mapping Settings<br /> <br /> Add and Edit
					fields below if you want to map any WordPress fields to your
					Content Assests
				</th>
			</tr>
			<tr>
				<td colspan='2'>
					<table>
						<tbody id="p_scents">
							<tr>
								<th scope="row">Salesforce Field</th>
								<th scope="row">WordPress Field</th>
								<th>&nbsp;</th>
							</tr>
					<?php
					if (count ( $dw_custom_meta ) > 0) {
						foreach ( $dw_custom_meta as $key => $value ) {
							
							echo '<tr><td scope="row" style="padding-left:0px;"><input type="text" id="p_scnt" size="20" name="dw_custom[]" value="' . (isset ( $key ) ? $key : "") . '" placeholder="Input Value" /></td> <td style="padding-left:0px;"><input type="text" name="wp_custom[]" size="20"   value="' . (isset ( $value ) ? $value : "") . '" ></td><td><a href="#" id="remScnt">Remove</a></td></tr>';
						}
					}
					if ($totalCustomRow <= 0)
						echo '<tr><td scope="row" style="padding-left:0px;"><input type="text" id="p_scnt" size="20" name="dw_custom[]" placeholder="Input Value" /></td> <td style="padding-left:0px;"><input type="text" size="20" name="wp_custom[]" placeholder="Input Value"></td><td><a href="#" id="remScnt">Remove</a></td></tr>';
					?>
						
					</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan='2' align="center"><a href="#" id="addScnt">Add Another
						Custom Field</a></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="update_mapping_meta"
					value="Save mappings" class="button-primary" /></td>
			</tr>
		</tbody>
	</table>
</form>


<script>
jQuery(function() {
    var scntDiv = jQuery('#p_scents');  
    var i=1;
    jQuery('#addScnt').live('click', function() {    	
    	jQuery('<tr><td scope="row"><input type="text" id="p_scnt" size="20" name="dw_custom[]" value="" placeholder="Input Value" /></td><td scope="row"><input type="text" id="p_scnt" size="20" name="wp_custom[]" value="" placeholder="Input Value" /></td> <td><a href="#" id="remScnt">Remove</a></td></tr>').appendTo(scntDiv);
            i++;
            return false;
    });
    
    jQuery('#remScnt').live('click', function() { 
                
            	jQuery(this).closest('tr').remove();
                    i--;
        
            return false;
    });
});			

</script>