<?php
if( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Log_List_Table extends WP_List_Table {
	
	public function __construct(){
		global $status, $page;

		parent::__construct( array(
				'singular'  => __( 'log', 'logtable' ),    //singular name of the listed records
				'plural'    => __( 'log', 'logtable' ),   //plural name of the listed records
				'ajax'      => false                     //does this table support ajax?
		) );
	}
	
	public function extra_tablenav( $which ) {
		global $wpdb, $testiURL, $tablename, $tablet;
		$log_table = $wpdb->prefix . 'demadware_log';
		if ( $which == "top" )
		{
		?>
	        <div class="alignleft actions bulkactions">
		        <?php
		        $status = $wpdb->get_results('select distinct status from '.$log_table, ARRAY_A);
		        if( $status )
		        {
		       	?>
			       	<form method="get">
			       		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>">
			       		<?php 
			       		if(isset($_GET['s']))
			       		{
			       		?>
			       			<input type="hidden" name="s" value="<?php echo $_GET['s'] ?>">
			       		<?php 
			       		}
			       		?>
			            <select name="status-filter" class="filter-status">
			                <option value="">Filter by Status</option>
			                <?php
			                $selected=$_GET['status-filter'];
			                foreach( $status as $stat )
			                {
			                ?>
			                	<option <?php if ($stat['status'] == $selected) echo 'selected'; ?> value="<?php echo $stat['status'];?>"><?php echo ($stat['status']=="1"?"Success":"Failure");?></option>
			                <?php   
			                }
			        		?>
			            </select>
			            <input type="submit" value="Filter" name="status-submit" class="button"/>
			        </form>
		        <?php 
		        }
		        ?>
	        </div>
	      
	    <?php
	    }
	    if ( $which == "bottom" )
	    {
	    ?>
	      <div class="alignleft actions bulkactions">
	      	<form method="post">
	        	<input type="submit" name="empty-log" value="Empty Log" class="button"/>
	        </form>
	      </div>
	    <?php 	
	    }
	}
	
	public function process_empty_log()
	{
		global $wpdb;
		$log_table = $wpdb->prefix . 'demadware_log';
		
		if (isset($_POST['empty-log'])) {
			$wpdb->query("TRUNCATE table ".$log_table);
			echo "<script>location.reload();</script>";
		}
	}
	
	private function table_data()
	{
		add_thickbox();

		global $wpdb;
		$log_table = $wpdb->prefix . 'demadware_log';
		$users_table = $wpdb->prefix .'users';
		$data = array();

		$orderby = !empty($_GET["orderby"]) ? $_GET["orderby"] : 'log_id';
		$order = !empty($_GET["order"]) ? $_GET["order"] : 'DESC';
		if(!empty($orderby) & !empty($order))
		{ 
			$query =" ORDER BY ".$orderby." ".$order; 
		}
		
		if(isset($_GET['s']) && $_GET['s']!="" && isset($_GET['status-filter']) && $_GET['status-filter']!="")
		{
			$search = trim($_GET['s']);
			$search_status = $_GET['status-filter'];
			
			$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".$log_table." WHERE status = %d AND (event LIKE '%%%s%%' OR user_id in(select ID from dwp_users join dwp_demadware_log on dwp_users.ID=dwp_demadware_log.user_id and user_login LIKE '%%%s%%') OR server_id in(select ID from dwp_posts join dwp_demadware_log on dwp_posts.ID=dwp_demadware_log.server_id and post_title LIKE '%%%s%%'))".$query, $search_status,$search,$search,$search));			
		}
		else if(isset($_GET['s']) && $_GET['s']!="")
		{
			$search = trim($_GET['s']);
			$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".$log_table." WHERE event LIKE '%%%s%%' OR user_id in(select ID from dwp_users join dwp_demadware_log on dwp_users.ID=dwp_demadware_log.user_id and user_login LIKE '%%%s%%') OR server_id in(select ID from dwp_posts join dwp_demadware_log on dwp_posts.ID=dwp_demadware_log.server_id and post_title LIKE '%%%s%%')".$query, $search,$search,$search));
		}
		else if(isset($_GET['status-filter']) && $_GET['status-filter']!="")
		{
			$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM ".$log_table." WHERE status = %d".$query, $_GET['status-filter']));
		}
		else 
		{
			$results = $wpdb->get_results('SELECT * FROM '.$log_table.$query);
		}
		
		$cnt=0;
		foreach($results as $key => $value)
		{
			$user_name = $wpdb->get_var('SELECT user_login FROM '.$users_table .' INNER JOIN '. $log_table .' ON '.$users_table.'.ID='.$log_table.'.user_id AND '.$users_table.'.ID='.$value->user_id);
			$server_name = get_the_title($value->server_id);
			$server_address = get_post_meta($value->server_id,"dw_server_address",true);
				
		  	$data[] = array(
		  				'log_id'        => $value->log_id,
		  				'user_name'     => '<a href="'.get_edit_user_link( $value->user_id ).'">'.$user_name.'</a>',		  				
		  				'post_id'       => '<a href="'.get_edit_post_link( $value->post_id ).'">'.$value->post_id.'</a>',
		  				'event'         => $value->event,
		  				'server_name'   => '<a href="'.get_edit_post_link( $value->server_id ).'">'.$server_name.'</a>',
		  				'server_address'=> $server_address,
		  				'status'        => ($value->status == 0)?'Failure':'Success',
		  				'response'      => '<a href="#TB_inline?width=600&height=550&inlineId=my-response-'.$cnt.'" class="thickbox">View</a>',
		  				'log_time'		=> $value->log_time
		  		);
		  	
		  	?>
			<div id="my-response-<?php echo $cnt;?>" style="display:none;">
				<p>
				<?php 
					echo "<pre>";
					print_r(json_decode($value->response,true));
					echo "</pre>";
				?>
				</p>
			</div>
		  	<?php
		  	$cnt++;
		}
		return $data;
	} 
	
	public function column_default( $item, $column_name ) {
		switch( $column_name ) {
			case 'log_id':
			case 'user_name':
			case 'post_id':
			case 'event':			
			case 'server_name':
			case 'server_address':
			case 'status':
			case 'response':
			case 'log_time':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
		}
	}

	public function get_columns(){
		$columns = array(
				'log_id'		=> __( 'Log ID',  'logtable' ),
				'user_name' 	=> __( 'User', 	  'logtable' ),
				'post_id' 		=> __( 'Post ID', 'logtable' ),
				'event' 		=> __( 'Event',   'logtable' ),
				'server_name'	=> __( 'Server Name',  'logtable' ),
				'server_address'=> __( 'Server Address',  'logtable' ),
				'status' 		=> __( 'Status',  'logtable' ),
				'response' 		=> __( 'Response','logtable' ),
				'log_time' 		=> __( 'Log Time','logtable' ),
		);
		return $columns;
	}
	
	public function no_items() {
		_e( 'No log found.' );
	}
	
	public function prepare_items() {
		$found_data =array();
		$columns  = $this->get_columns();
		$table_data = $this->table_data();
		$hidden   = array();
		$sortable =  $this->get_sortable_columns();
		$this->_column_headers = array( $columns, $hidden, $sortable );
		
		$per_page = 10;
		$current_page = $this->get_pagenum();
		$total_items = count( $table_data );
		
		$found_data = array_slice( $table_data,( ( $current_page-1 )* $per_page ), $per_page );
		
		$this->set_pagination_args( array(
				'total_items' => $total_items,                  
				'per_page'    => $per_page                    
		) );
		
		$this->items = $found_data;
		
	}
	
	public function get_sortable_columns() {
		$sortable_columns = array(
				'log_id'  	=> array('log_id',false),
				'status'   	=> array('status',false),
				'log_time'  => array('log_time',false)
		);
		return $sortable_columns;
	}

} //class
?>
<div class="wrap">
<h1>Log</h1>
<?php 
$myLogTable = new Log_List_Table();
$myLogTable->prepare_items();
?>
<form method="get">
<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>">
<?php $myLogTable->search_box( 'search', 'log_search_id' ); ?>
</form>
<?php 
$myLogTable->display();
$myLogTable->process_empty_log(); 
?>
</div>