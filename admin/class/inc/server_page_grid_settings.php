<?php
add_filter( 'manage_edit-dw-servers_columns', 'edit_server_columns' ) ;

function edit_server_columns( $columns ) {

	$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Server Name' ),
			'dw_server_address' => __( 'Server Address' ),
			'dw_access_token' => __( 'Access Token' ),
			'dw_server_status' => __( 'Status' ),			
	);

	return $columns;
}

add_action( 'manage_dw-servers_posts_custom_column', 'manage_server_columns', 10, 2 );

function manage_server_columns( $column, $post_id ) {
	
	global $post;

	switch( $column ) {
		
		case 'dw_access_token' :
			$accessToken = get_post_meta( $post_id, 'dw_access_token', true );
			if ( empty( $accessToken ) )
				echo __( '-' );
			else
				printf( __( '%s' ), $accessToken );
			break;
					
		case 'dw_server_status' :
			$serverStatus = get_post_meta( $post_id, 'dw_server_status',true);	
			if ( empty( $serverStatus ) )
				echo __( 'Unknown' );
			else
				printf( __( '%s' ), $serverStatus);
			break;
		
		case 'dw_server_address' :
			$serverAddress = get_post_meta( $post_id, 'dw_server_address',true);
			if ( empty( $serverAddress ) )
				echo __( 'Unknown' );
			else
				printf( __( '%s' ), $serverAddress);
			break;
		
		default :
			break;
	}		
}

function serverStatusRequestAdmin($request) {
	if( isset($_GET['dw_server_status']) && !empty($_GET['dw_server_status']) ) {
		$request['meta_key'] = 'dw_server_status';
		$request['meta_value'] = $_GET['dw_server_status'];
	}
	return $request;
}

function serverStatusRestrictManagePosts() {
	global $wpdb;
	$statuses = $wpdb->get_col("
        SELECT DISTINCT meta_value
        FROM ". $wpdb->postmeta ."
        WHERE meta_key = 'dw_server_status'
        ORDER BY meta_value
    ");
	?>   
    <select name="dw_server_status" id="dw_server_status">
        <option value="">All Status</option>
        <?php foreach ($statuses as $status) { ?>
        <option value="<?php echo esc_attr( $status ); ?>" <?php if(isset($_GET['dw_server_status']) && !empty($_GET['dw_server_status']) ) selected($_GET['dw_server_status'], $status); ?>><?php echo ucfirst(esc_attr($status)); ?></option>
        <?php } ?>
    </select>
    <?php
}

if( is_admin() && isset($_GET['post_type']) && $_GET['post_type'] == 'dw-servers' ) {
    add_filter('request', 'serverStatusRequestAdmin');
    add_filter('restrict_manage_posts', 'serverStatusRestrictManagePosts');
}