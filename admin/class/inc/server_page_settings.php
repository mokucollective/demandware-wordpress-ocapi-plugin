<?php 
$labels = array(
			'name'               => _x( 'Servers', 'post type general name', '' ),
			'singular_name'      => _x( 'Server', 'post type singular name', '' ),
			'menu_name'          => _x( 'Servers', 'admin menu', '' ),
			'name_admin_bar'     => _x( 'Server', 'add new on admin bar', '' ),
			'add_new'            => _x( 'Add New', 'Server', '' ),
			'add_new_item'       => __( 'Add New Server', '' ),
			'new_item'           => __( 'New Server', '' ),
			'edit_item'          => __( 'Edit Server', '' ),
			'view_item'          => __( 'View Server', '' ),
			'all_items'          => __( 'Server Settings', '' ),
			'search_items'       => __( 'Search Servers', '' ),
			'parent_item_colon'  => __( 'Parent Servers:', '' ),
			'not_found'          => __( 'No servers found.', '' ),
			'not_found_in_trash' => __( 'No servers found in Trash.', '' )
		);
	
$args = array(
			'labels'             => $labels,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'		 => 'dw-general-settings-page',
			'query_var'          => false,
			'rewrite'            => array( 'slug' => 'dw-servers' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array('title')
		);
	
/**
 * Register servers post type.
*/
register_post_type( 'dw-servers', $args );

flush_rewrite_rules();

add_action( 'add_meta_boxes', 'add_servers_metaboxes' );

function add_servers_metaboxes()
{
	add_meta_box( 'dw-servers-metaboxes', 'Server Configuration', 'dw_servers_metaboxes_cb', 'dw-servers', 'normal', 'high' );
}

function dw_servers_metaboxes_cb( $post )
{
	wp_nonce_field(plugin_basename(__FILE__), 'dw_servers_nonce');
	
	$values = get_post_custom( $post->ID );
	
	$server_address = isset( $values['dw_server_address'][0] ) ? esc_attr( $values['dw_server_address'][0]) :'';
	$client_id = isset( $values['dw_client_id'][0] ) ? esc_attr( $values['dw_client_id'][0]) :'';
	$client_secret = isset( $values['dw_client_secret'][0] ) ? esc_attr( $values['dw_client_secret'][0]) :'';
	$access_token = isset( $values['dw_access_token'][0] ) ? esc_attr( $values['dw_access_token'][0]) :'';
	$access_token_refresh_time = isset( $values['dw_access_token_refresh_time'][0] ) ? esc_attr( $values['dw_access_token_refresh_time'][0]) :'25';
	$library_id = isset( $values['dw_library_id'][0] ) ? esc_attr( $values['dw_library_id'][0]) :'';
	$base_folder = isset( $values['dw_base_folder'][0] ) ? esc_attr( $values['dw_base_folder'][0]) :'';
	$content_id_prefix = isset( $values['dw_content_id_prefix'][0] ) ? esc_attr( $values['dw_content_id_prefix'][0]) :'';
	$server_status = isset( $values['dw_server_status'][0] ) ? esc_attr( $values['dw_server_status'][0]) :'enable';
	$audit_log = isset( $values['dw_audit_log'][0] ) ? esc_attr( $values['dw_audit_log'][0]) :'90';
	
	?>
	<table class="form-table">
	<tbody>
		<tr>
			<th scope="row"><label for="dw_server_address">Server Address</label></th>
			<td><input type="text" name="dw_server_address" id="dw_server_address" class="regular-text" value="<?php echo $server_address; ?>" required /></td>
		</tr>
		
	    <tr>
	    	<th scope="row"><label for="dw_client_id">Client ID</label></th>
	    	<td><input type="text" name="dw_client_id" id="dw_client_id" class="regular-text" value="<?php echo $client_id; ?>" required /></td>
	    </tr>
	    
	    <tr>
	    	<th scope="row"><label for="dw_client_secret">Client Secret</label></th>
	    	<td><input type="text" name="dw_client_secret" id="dw_client_secret" class="regular-text" value="<?php echo $client_secret; ?>" required /></td>
	    </tr>
	    
	    <tr>
	    	<th scope="row"><label for="dw_access_token">Access Token</label></th>
	    	<td><input type="text" name="dw_access_token" id="dw_access_token" class="regular-text" value="<?php echo $access_token; ?>" disabled /></td>
	    </tr>
	    
	    <tr>
	    	<th scope="row"><label for="dw_access_token_refresh_time">Access Token Refresh Time</label></th>
	    	<td><input type="number" name="dw_access_token_refresh_time" id="dw_access_token_refresh_time" class="small-text" value="<?php echo $access_token_refresh_time; ?>" min="0" /> Minutes</td>
	    </tr>
	    
	    <tr>
	    	<th scope="row"><label for="dw_library_id">Library ID</label></th>
	    	<td><input type="text" name="dw_library_id" id="dw_library_id" class="regular-text" value="<?php echo $library_id; ?>" required /><a class="tooltips" href="#"><span>Specify Library ID e.g Moku </span></a></td>
	    </tr>
	    
	     <tr>
	    	<th scope="row"><label for="dw_base_folder">Base Folder</label></th>
	    	<td><input type="text" name="dw_base_folder" id="dw_base_folder" class="regular-text" value="<?php echo $base_folder; ?>" required /><a class="tooltips" href="#"><span>Specify Library folder e.g Folder1 </span></a></td>
	    </tr>
	    
	     <tr>
	    	<th scope="row"><label for="dw_content_id_prefix">Content ID Prefix</label></th>
	    	<td><input type="text" name="dw_content_id_prefix" id="dw_content_id_prefix" class="regular-text" value="<?php echo $content_id_prefix; ?>" /><a class="tooltips" href="#"><span>Specify Content ID Prefix for page/post </span></a></td>
	    </tr>
	    
	     <tr>
	    	<th scope="row"><label for="dw_server_status">Server Status</label></th>
	    	<td>
		    	<select name="dw_server_status" id="dw_server_status">
		    		<option value="Enabled" <?php selected( $server_status, 'Enabled' ); ?>>Enabled</option>
		    		<option value="Disabled" <?php selected( $server_status, 'Disabled' ); ?>>Disabled</option>
		    	</select>
	    	</td>
	    </tr>
	    
	     <tr>
	    	<th scope="row"><label for="dw_audit_log">Audit Log Retention</label></th>
	    	<td><input type="number" name="dw_audit_log" id="dw_audit_log" class="small-text" value="<?php echo $audit_log; ?>" min="0"/> Days</td>
	    </tr>
    </tbody>
    </table>
 	<?php        
}

add_action( 'save_post', 'dw_servers_metaboxes_save' );
function dw_servers_metaboxes_save( $post_id )
{
	
	// Bail if we're doing an auto save
    
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;
    
    // Check if our nonce is set.
    if ( ! isset( $_POST['dw_servers_nonce'] ) ) {
    	return $post_id;
    }
    
    // if our nonce isn't there, or we can't verify it, bail
    if(!wp_verify_nonce($_POST['dw_servers_nonce'], plugin_basename(__FILE__))) {
    	return $post_id;
    }
    
    // if our current user can't edit this post, bail
    //if( !current_user_can( 'edit_post' ) ) return $post_id;
    if ( ! current_user_can( 'edit_post', $post_id ) )
    	return;
    
    if( isset( $_POST['dw_server_address'] ) && !empty($_POST['dw_server_address']) &&  isset( $_POST['dw_client_id'] ) &&  !empty($_POST['dw_client_id']) &&  isset( $_POST['dw_client_secret'] ) &&  !empty($_POST['dw_client_secret']))
    {
    	$ocapi= new OCAPI($_POST['dw_client_id'],$_POST['dw_client_secret'],"https://account.demandware.com/dw/oauth2/access_token");    	
    	$ocapi->checkTokenRequestSingleServer($post_id);
    }
         
    // Make sure your data is set before trying to save it
    if( isset( $_POST['dw_server_address'] ) )
        update_post_meta( $post_id, 'dw_server_address', sanitize_text_field( $_POST['dw_server_address']) );
   
    if( isset( $_POST['dw_client_id'] ) )
    	update_post_meta( $post_id, 'dw_client_id', sanitize_text_field( $_POST['dw_client_id']) );
    
    if( isset( $_POST['dw_client_secret'] ) )
    	update_post_meta( $post_id, 'dw_client_secret', sanitize_text_field( $_POST['dw_client_secret']) );
    
    if( isset( $_POST['dw_access_token'] ) )
    	update_post_meta( $post_id, 'dw_access_token', sanitize_text_field( $_POST['dw_access_token']) );
    
    if( isset( $_POST['dw_access_token_refresh_time']) && !empty( $_POST['dw_access_token_refresh_time'] ))
    	update_post_meta( $post_id, 'dw_access_token_refresh_time', sanitize_text_field( $_POST['dw_access_token_refresh_time']) );
    else
    	update_post_meta( $post_id, 'dw_access_token_refresh_time', '25');
    
    if( isset( $_POST['dw_library_id'] ) )
    	update_post_meta( $post_id, 'dw_library_id', sanitize_text_field( $_POST['dw_library_id']) );
    
    if( isset( $_POST['dw_base_folder'] ) )
    	update_post_meta( $post_id, 'dw_base_folder', sanitize_text_field( $_POST['dw_base_folder']) );
    
    if( isset( $_POST['dw_content_id_prefix'] ) )
    	update_post_meta( $post_id, 'dw_content_id_prefix', sanitize_text_field( $_POST['dw_content_id_prefix']) );
    
    if( isset( $_POST['dw_server_status'] ) )
    	update_post_meta( $post_id, 'dw_server_status', $_POST['dw_server_status']);
    
    if( isset( $_POST['dw_audit_log'] ) && !empty( $_POST['dw_audit_log'] ))
    	update_post_meta( $post_id, 'dw_audit_log', sanitize_text_field( $_POST['dw_audit_log']) );
    else 
    	update_post_meta( $post_id, 'dw_audit_log', '90');   
}

add_filter( 'gettext', 'change_publish_button_text', 10, 2 );

function change_publish_button_text( $translation, $text ) {
	if ( 'dw-servers' == get_post_type())
	{
		if ( $text == 'Publish' )
			return 'Save Server';
		if ( $text == 'Update' )
			return 'Update Server';
	}
	return $translation;
}
?>