<?php
class demandWareMenu {
	/**
	 *
	 * Unique identifier for your plugin.
	 *
	 *
	 * For easier overriding we declared the keys
	 * here as well as our tabs array which is populated
	 * when registering settings
	 *
	 * @since 1.0.0
	 * @author Weblineindia
	 *        
	 *        
	 */
	private $dw_meta_settings_key = 'mapping_settings';
	private $dw_custom_meta_settings_key = 'custom_mapping_settings';
	private $dwMeta = array(
		"id" => array(
			'input_val' => 'post_id',
			'tooltip' => ''
		) ,
		"name" => array(
			'input_val' => 'post_title',
			'tooltip' => 'Name of the Post'
		) ,
		"description" => array(
			'input_val' => 'post_description',
			'tooltip' => ''
		) ,
		"online" => array(
			'input_val' => 'post_status',
			'tooltip' => 'Status field of post'
		) ,
		"page_title" => array(
			'input_val' => 'post_title',
			'tooltip' => ''
		) ,
		"page_description" => array(
			'input_val' => 'post_description',
			'tooltip' => ''
		),
		"page_keywords" => array(
				'input_val' => 'page_keywords',
				'tooltip' => ''
		),
		"page_url" => array(
				'input_val' => 'page_url',
				'tooltip' => ''
		),
		"link" => array(
				'input_val' => 'link',
				'tooltip' => 'The link to the content asset resource.'
		),			
		"online" => array(
				'input_val' => 'online',
				'tooltip' => ''
		),			
		"searchable" => array(
				'input_val' => 'searchable',
				'tooltip' => ''
		),
		"site_map_change_frequency" => array(
				'input_val' => 'site_map_change_frequency',
				'tooltip' => ''
		),	
		"site_map_included" => array(
					'input_val' => 'site_map_included',
					'tooltip' => ''
		),	
		"site_map_priority" => array(
					'input_val' => 'site_map_priority',
					'tooltip' => ''
		),	
		"template" => array(
					'input_val' => 'template',
					'tooltip' => ''
		)	
	);
	
	private $dwCustomMeta = array();
	
	
	public function __construct() {
		$this->load_settings ();
		add_action ( 'init', array (
				&$this,
				'server_page_settings'
		) );
		add_action ( 'admin_menu', array (
				&$this,
				'setup_theme_admin_menus'
		) );
		add_action ( 'add_meta_boxes', array (
				&$this,
				'add_meta_to_post'
		) );
	}	
	/**
	 * Loads settings from
	 * the database into their respective arrays.
	 * Uses
	 * array_merge to merge with default values if they're
	 * missing.
	 *
	 * @since 1.0.0
	 * @var No arguments passed
	 * @return void
	 * @author Weblineindia
	 */
	function load_settings() {	
		//echo "<pre>"; print_r($this->dwMeta->id->input_val); die;
		$this->mapping_settings = get_option($this->dw_meta_settings_key);
		$this->custom_mapping_settings = get_option($this->dw_custom_meta_settings_key);
		//print_r($this->mapping_settings);
		define ( "DW_META_KEY", $this->mapping_settings);
		define ( "DW_CUSTOM_META_KEY", $this->custom_mapping_settings);
	}
	
	function add_meta_to_post(){
		require_once 'inc/add_meta_to_post.php';		
	}
	
	function setup_theme_admin_menus() {
		
		add_menu_page('Moku OCAPI Settings', 
					  'Moku OCAPI', 
					  'manage_options',
					  'dw-general-settings-page', 
					  array (&$this,'demandware_ocapi_settings_page'),
					  'dashicons-album'
		);
					
		add_submenu_page('dw-general-settings-page',
						 'Field Mapping', 
						 'Field Mapping', 
						 'manage_options',
						 'dw-field-mapping-page', 
						 array (&$this,'field_mapping_page_settings'));
		
		add_submenu_page('dw-general-settings-page',
						 'Log', 
						 'Log', 
						 'manage_options',
						 'dw-log-page', 
						 array (&$this,'log_page_settings'));
	}
	
	function demandware_ocapi_settings_page(){}
	
	function server_page_settings(){
		require_once 'inc/server_page_settings.php';
		require_once 'inc/server_page_grid_settings.php';
	}
	
	function field_mapping_page_settings(){
		require_once 'inc/field_mapping_page_settings.php';
	}
	
	function configuration_page_settings(){
		require_once 'inc/configuration_page_settings.php';
	}
	
	function log_page_settings(){
		require_once 'inc/log_page_settings.php';
	}
}
new demandWareMenu ();