<?php
class OCAPIException extends Exception
{
	const CURL_NOT_FOUND                     = 0x01;
	const CURL_ERROR                         = 0x02;
	const GRANT_TYPE_ERROR                   = 0x03;
	const INVALID_CLIENT_AUTHENTICATION_TYPE = 0x04;
	const INVALID_ACCESS_TOKEN_TYPE          = 0x05;
}
class OCAPI {
	/**
	 *
	 * Unique identifier for your plugin.
	 *
	 *
	 * For easier overriding we declared the keys
	 * here as well as our tabs array which is populated
	 * when registering settings
	 *
	 * @since 1.0.0
	 * @author Weblineindia
	 *
	 *
	 */
	
	private $clientId;
	private $clientSecret;
	private $accessTokenUrl;
	
	/**
	 * HTTP Methods
	 */
	const HTTP_METHOD_GET    = 'GET';
	const HTTP_METHOD_POST   = 'POST';
	const HTTP_METHOD_PUT    = 'POST';
	const HTTP_METHOD_DELETE = 'DELETE';
	const HTTP_METHOD_HEAD   = 'HEAD';
	const HTTP_METHOD_PATCH  = 'PATCH';
	
	/**
	 * HTTP Form content types
	 */
	const HTTP_FORM_CONTENT_TYPE_APPLICATION = 0;
	const HTTP_FORM_CONTENT_TYPE_MULTIPART = 1;
	
	public function __construct($client_id,$client_secret,$access_token_url){
		if (!extension_loaded('curl')) {
			throw new OCAPIException('The PHP exention curl must be installed to use this library.', OCAPIException::CURL_NOT_FOUND);
		}
		
		$this->clientId=$client_id;
		$this->clientSecret=$client_secret;
		$this->accessTokenUrl=$access_token_url;				
	}
	
	public function getAccessToken(){
		$arr=array(
				'grant_type'=>'client_credentials',
				'client_id'=>$this->clientId,
				'client_secret'=>$this->clientSecret				
		);								
		return $this->executeRequest($this->accessTokenUrl, $arr, self::HTTP_METHOD_POST, null , self::HTTP_FORM_CONTENT_TYPE_APPLICATION);
	}
	
	public function pushSinglePost($accessToken,$arr, array $commonSettings=array()){		
		$post_array=json_decode($arr);
				
		$arr=json_encode($post_array);
		
		$headerArr=array(
				"Authorization"=>"Bearer ".$accessToken,
				"Accept"=>"application/json",
				"Content-Type"=>"application/json",
				"x-dw-http-method-override"=>"PUT"
		);
		
		$pushResponse = array();
		
		$resCreateFile = $this->executeRequest($commonSettings['serverAddress']."/s/-/dw/data/v15_4/libraries/".$commonSettings['libraryId']."/content/".$post_array->id, $arr, self::HTTP_METHOD_PUT, $headerArr , "application/json");			
		if(!isset($resCreateFile['result']['fault']))
		{
			$resAssignFile = $this->executeRequest($commonSettings['serverAddress']."/s/-/dw/data/v15_4/libraries/".$commonSettings['libraryId']."/folder_assignments/".$post_array->id."/".$commonSettings['baseFolder'], '', self::HTTP_METHOD_PUT, $headerArr , "application/json");			
			if(!isset($resAssignFile['result']['fault']))
			{
				$pushResponse['responseStatus']=1;
				$pushResponse['response']=$resAssignFile;				
			}
			else 
			{
				$pushResponse['responseStatus']=0;
				$pushResponse['response']=$resAssignFile;
				$pushResponse['responseAlert']=$resAssignFile['result']['fault']['message'];
			}
		}	
		else 
		{
			$pushResponse['responseStatus']=0;
			$pushResponse['response']=$resCreateFile;
			$pushResponse['responseAlert']=$resCreateFile['result']['fault']['message'];
		}			
		$pushResponse['responseTime']=date('Y-m-d H:i:s');
		return $pushResponse;
	}
	
	/**
	 * Get the client Id
	 *
	 * @return string Client ID
	 */
	public function getClientId(){
		return $this->clientId;
	}
	
	/**
	 * Get the client Secret
	 *
	 * @return string Client Secret
	 */
	public function getClientSecret(){
		return $this->clientSecret;
	}
	
	/**
	 * getAuthenticationUrl
	 *
	 * @param string $auth_endpoint Url of the authentication endpoint
	 * @param string $redirect_uri  Redirection URI
	 * @param array  $extra_parameters  Array of extra parameters like scope or state (Ex: array('scope' => null, 'state' => ''))
	 * @return string URL used for authentication
	 */
	public function getAuthenticationUrl($auth_endpoint, $redirect_uri, array $extra_parameters = array()){
		$parameters = array_merge(array(
				'response_type' => 'code',
				'client_id'     => $this->client_id,
				'redirect_uri'  => $redirect_uri
		), $extra_parameters);
		return $auth_endpoint . '?' . http_build_query($parameters, null, '&');
	}
	
	/**
	 * Execute a request (with curl)
	 *
	 * @param string $url URL
	 * @param mixed  $parameters Array of parameters
	 * @param string $http_method HTTP Method
	 * @param array  $http_headers HTTP Headers
	 * @param int    $form_content_type HTTP form content type to use
	 * @return array
	 */
	private function executeRequest($url, $parameters = array(), $http_method = self::HTTP_METHOD_GET, array $http_headers = null, $form_content_type = self::HTTP_FORM_CONTENT_TYPE_MULTIPART){	
		$curl_options = array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => true,
				CURLOPT_CUSTOMREQUEST  => $http_method,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_SSL_VERIFYHOST => 0
		);		
		switch($http_method) {			
			case self::HTTP_METHOD_POST:				
				$curl_options[CURLOPT_POST] = true;
				/* No break */
			case self::HTTP_METHOD_PUT:
				
			case self::HTTP_METHOD_PATCH:				
				/**
				 * Passing an array to CURLOPT_POSTFIELDS will encode the data as multipart/form-data,
				 * while passing a URL-encoded string will encode the data as application/x-www-form-urlencoded.
				 * http://php.net/manual/en/function.curl-setopt.php
				 */
				if(is_array($parameters) && self::HTTP_FORM_CONTENT_TYPE_APPLICATION === $form_content_type) {
					$parameters = http_build_query($parameters, null, '&');
				}				
				$curl_options[CURLOPT_POSTFIELDS] = $parameters;
				break;
			case self::HTTP_METHOD_HEAD:
				$curl_options[CURLOPT_NOBODY] = true;
				/* No break */
			case self::HTTP_METHOD_DELETE:
			case self::HTTP_METHOD_GET:
				if (is_array($parameters)) {
					$url .= '?' . http_build_query($parameters, null, '&');
				} elseif ($parameters) {
					$url .= '?' . $parameters;
				}
				break;
			default:
				break;
		}
	
		$curl_options[CURLOPT_URL] = $url;		
		if (is_array($http_headers)) {
			$header = array();
			foreach($http_headers as $key => $parsed_urlvalue) {
				$header[] = "$key: $parsed_urlvalue";
			}
			$header['x-dw-http-method-override'] = "PUT";
			$curl_options[CURLOPT_HTTPHEADER] = $header;
		}			
		$ch = curl_init();			
		curl_setopt_array($ch, $curl_options);				
		$result = curl_exec($ch);	
			
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		
		if ($curl_error = curl_error($ch)) {
			throw new OCAPIException($curl_error, OCAPIException::CURL_ERROR);
		} else {
			$json_decode = json_decode($result, true);
		}
		curl_close($ch);	
		return array(
				'result' => (null === $json_decode) ? $result : $json_decode,
				'code' => $http_code,
				'content_type' => $content_type
		);
	}
	
	function checkTokenRequestSingleServer($post_id){	
		if(get_post_meta($post_id,"dw_access_token",true)!=null && get_post_meta($post_id,"dw_access_token_created_time",true)!=null)
		{								
			$datetime1 = strtotime(get_post_meta($post_id,"dw_access_token_created_time",true));
			$datetime2 = strtotime(date('Y-m-d H:i:s'));
			$interval  = abs($datetime2 - $datetime1);
			$token_minutes   = round($interval / 60);				
			if($token_minutes >= get_post_meta($post_id,"dw_access_token_refresh_time",true))
			{
				$tokenRequest=$this->getAccessToken();													
				if(!isset($tokenRequest['result']['error_description']) || !isset($tokenRequest['result']['error']))
				{													
					update_post_meta( $post_id, 'dw_access_token', $tokenRequest['result']['access_token']);
					update_post_meta( $post_id, 'dw_access_token_created_time', date('Y-m-d H:i:s'));
				}					
			}							
			return get_post_meta($post_id,"dw_access_token",true);
		}
		else
		{				
			$tokenRequest=$this->getAccessToken();				
			if(!isset($tokenRequest['result']['error_description']) || !isset($tokenRequest['result']['error']))
			{
				update_post_meta( $post_id, 'dw_access_token', $tokenRequest['result']['access_token']);
				update_post_meta( $post_id, 'dw_access_token_created_time', date('Y-m-d H:i:s'));
			}
			return get_post_meta($post_id,"dw_access_token",true);
		}	
	}	
}
