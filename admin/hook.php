<?php
require 'class/ocapi.php';

class demandAdminWareHooks {
    	
    public function __construct() {          	    	           	
    	
    	add_action( 'admin_head', array( $this, 'admin_header' ) );
    	
        add_action('wp_ajax_nopriv_get_post_data', array($this,'get_post_data'));
        add_action('wp_ajax_get_post_data', array($this,'get_post_data'));
        
        add_action('wp_ajax_nopriv_push_all', array($this,'push_all'));
        add_action('wp_ajax_push_all', array($this,'push_all'));
    }
    
    public function admin_header()
    {
    	$page = ( isset($_GET['page'] ) ) ? esc_attr( $_GET['page'] ) : false;
    	if( 'dw-log-page' != $page )
    		return;
    	echo '<style type="text/css">';
    	echo '.wp-list-table .column-log_id { width: 8%; }';
    	echo '.wp-list-table .column-user_name { width: 10%; }';
    	echo '.wp-list-table .column-post_id { width: 9%; }';
    	echo '.wp-list-table .column-event { width: 15%; }';    	
    	echo '.wp-list-table .column-server_name { width: 10%;}';
    	echo '.wp-list-table .column-server_address { width: 20%;}';
    	echo '.wp-list-table .column-status { width: 8%;}';
    	echo '.wp-list-table .column-response { width: 8%;}';
    	echo '.wp-list-table .column-log_time { width: 12%;}';
    	echo '.tablenav .actions{ padding:0px;}';
    	echo '</style>';
    }
     
    public function get_mapped_data($post_id,$contentIdPrefix) {
    	$preserveArr = json_decode (  DW_META_KEY );
    	$preserveCustomArr = json_decode(DW_CUSTOM_META_KEY);
    	$content_post = get_post($post_id);
    	$post_tags = wp_get_post_tags($post_id, array( 'fields' => 'names' ));
    	$post_tags_str = implode (", ", $post_tags);
    	$object = new stdClass();
    	//print_r($preserveCustomArr); die;
    	foreach($preserveArr as $key=>$val)
    	{   	
    	
    		if(!empty($val))
    		{
	    		switch($val)
	    		{
	    			case "post_id":
	    				$object->$key = $contentIdPrefix.$content_post->ID;	
	    				break;
	
	    			case "post_title":
	    				$object->$key = array('default'=>$content_post->post_title);    				
	    				break;
	
	    			case "post_description":
	    				$object->$key = array('default'=>$content_post->post_content);    				
	    				break;
	    			
	    			case "post_status":
	    				$object->$key = array('default'=>($content_post->post_status=='publish')?true:false);    				
	    				break;  
  		
	    			case "post_tags":
    					$object->$key = array('default'=>$post_tags_str);
    					break;

	    			case strstr($val,"field:"):    				
	    				$metaKey=substr($val,strpos($val, ":")+1);
						$metaVal=get_post_meta($content_post->ID,$metaKey);
	    				$metaVal=((empty($metaVal[0]))? "" : $metaVal[0]);    				    			
	    				$object->$key = array('default'=>$metaVal);    				
	    				break;    			    			
	    		}
    		}    	
    	}
    	
    	foreach($preserveCustomArr as $key=>$val)
    	{
    		switch($val)
    		{
    			case "post_id":
    				$object->$key = $contentIdPrefix.$content_post->ID;
    				break;
    		
    			case "post_title":
    				$object->$key = array('default'=>$content_post->post_title);
    				break;
    		
    			case "post_description":
    				$object->$key = array('default'=>$content_post->post_content);
    				break;
    		
    			case "post_status":
    				$object->$key = array('default'=>($content_post->post_status=='publish')?true:false);
    				break;
    			
    			case "post_tags":
    				$object->$key = array('default'=>$post_tags_str);
    				break;
    					
    			default:
    				$object->$key = array('default'=>get_post_meta($content_post->ID,$val,true));
    				break;
    		}
    	}       	
    	return $object;
    }
    
    public function get_post_data() {
    	$html='';   
    	$msg = array(); 	    	
    	$postID = $_POST['postID'];
    	    	       	 
    	if(get_post_status ( $postID ) == "publish")
    	{ 
    		$serverId = $_POST['serverID'];
    		
    		$libraryId = get_post_meta($serverId,"dw_library_id",true);
    		$baseFolder = get_post_meta($serverId,"dw_base_folder",true);
    		$contentIdPrefix = get_post_meta($serverId,"dw_content_id_prefix",true);
    		$serverAddress = get_post_meta($serverId,"dw_server_address",true);
    		$clientId = get_post_meta($serverId,"dw_client_id",true);
    		$clientSecret = get_post_meta($serverId,"dw_client_secret",true);
    		
    		$commonSettings=array(
    				'libraryId'  => $libraryId,
    				'baseFolder' => $baseFolder,
    				'contentIdPrefix' => $contentIdPrefix,
    				'serverAddress' => $serverAddress
    		);
    		
    		$valuePostArr=json_encode($this->get_mapped_data($postID,$contentIdPrefix));	
    		
    		$ocapi = new OCAPI($clientId,$clientSecret,"https://account.demandware.com/dw/oauth2/access_token");
    			    		
	    	$tokenRequest = $ocapi->checkTokenRequestSingleServer($serverId); 
	    	  			    			    		
	    	$pushSingleResult = $ocapi->pushSinglePost($tokenRequest,$valuePostArr,$commonSettings);
	    					    		
	    	if($pushSingleResult['responseStatus'] == 1)
	    	{
	    		$msg['push_status'] = true;	    		
	    	}
	    	else
	    	{
	    		$msg['push_status'] = false;
	    		$msg['push_error'] = $pushSingleResult['responseAlert'];
	    	}
	    		
	    	$userId = get_current_user_id();
	    		
	    	global $wpdb;
	    	$table_name = $wpdb->prefix . 'demadware_log';
	    		
    		$insert=$wpdb->insert(
    				$table_name,
    				array(
    						'user_id' =>$userId,
    						'event'=>'Push Post : '.$contentIdPrefix.$postID,
    						'post_id' => $postID,
    						'server_id'=>$serverId,
    						'status'=>$pushSingleResult['responseStatus'],
    						'response'=>json_encode($pushSingleResult['response']),
    						'log_time'=>$pushSingleResult['responseTime']
    				),
    				array('%d','%s','%d','%d','%d','%s','%s')
    		);
	    	$msg['publish_status'] = true;
	    	$msg['previewLink']=$serverAddress.'/on/demandware.store/Sites-Site/default/ViewStorefront-Content?cid='.$contentIdPrefix.$postID;
    	}
    	else
    	{
    		$msg['publish_status'] = false;
    	}    
    	echo json_encode($msg);
    	die();
    }
    
    public function push_all() {    	
    	$html='';    	
    	$serverID= $_POST['serverID'];
    	$msg = array();   
    	$argsPost=array(
    			'post_type' => 'post',
    			'post_status' => 'publish',
    			'posts_per_page' => -1
    	);
    	$post_query = null;
    	$post_query = new WP_Query($argsPost);
    	if( $post_query->have_posts() ) {
    		while ($post_query->have_posts()) : $post_query->the_post();
   	
    		$postID = $post_query->post->ID;
			if(get_post_status ( $postID ) == "publish")
	    	{    	
    			$serverStatus = get_post_meta($serverID,"dw_server_status",true);
    			
    			if($serverStatus == "Enabled")
    			{
	    			$libraryId = get_post_meta($serverID,"dw_library_id",true);
	    			$baseFolder = get_post_meta($serverID,"dw_base_folder",true);
	    			$contentIdPrefix = get_post_meta($serverID,"dw_content_id_prefix",true);
	    			$serverAddress = get_post_meta($serverID,"dw_server_address",true);
	    			$clientId = get_post_meta($serverID,"dw_client_id",true);
	    			$clientSecret = get_post_meta($serverID,"dw_client_secret",true);
	    			
	    			$valuePostArr = json_encode($this->get_mapped_data($postID,$contentIdPrefix));    			
	    
	    			$commonSettings=array(
	    					'libraryId'  => $libraryId,
	    					'baseFolder' => $baseFolder,
	    					'contentIdPrefix' => $contentIdPrefix,
	    					'serverAddress' => $serverAddress
	    			);
	    		   
	    			$ocapi = new OCAPI($clientId,$clientSecret,"https://account.demandware.com/dw/oauth2/access_token");
	    			
	    			$tokenRequest = $ocapi->checkTokenRequestSingleServer($serverID);
	    			
	    			$pushSingleResult = $ocapi->pushSinglePost($tokenRequest,$valuePostArr,$commonSettings);
	    			  
	    			if($pushSingleResult['responseStatus'] == 1)
	    				$msg['push_status'] = true;
	    			else
	    				$msg['push_status'] = false;
	    			  
	    			$userId = get_current_user_id();
	    			  
	    			global $wpdb;
	    			$table_name = $wpdb->prefix . 'demadware_log';
	    			  
	    			$insert=$wpdb->insert(
	    					$table_name,
	    					array(
	    							'user_id' => $userId,
	    							'event' => 'Push All Post : '.$contentIdPrefix.$postID,
	    							'post_id' => $postID,
	    							'server_id' => $serverID,
	    							'status' => $pushSingleResult['responseStatus'],
	    							'response' => json_encode($pushSingleResult['response']),
	    							'log_time' => $pushSingleResult['responseTime']
	    					),
	    					array('%d','%s','%d','%d','%d','%s','%s')
	    			);
	    			$msg['server_status']= true;
    			}
    			else 
    			{
    				$msg['server_status']= false;
    			}
    			$msg['publish_status'] = true;
    		}
    		else
    		{
    			$msg['publish_status'] = false;
    		}
    		endwhile;
    	}
    	echo json_encode($msg);
    	die();
    }
    
}

new demandAdminWareHooks();
