<?php

class demandWareHooks {
    
    public function __construct() {          	    	
        register_activation_hook( DWO_PATH.DWO_PLUGIN_FILE, array($this, 'demandWareHook_activate') );
        register_deactivation_hook (DWO_PATH.DWO_PLUGIN_FILE, array( $this,'demandWareHook_deactivate'));
        register_uninstall_hook( DWO_PATH.DWO_PLUGIN_FILE, array( __CLASS__, 'demandWareHook_uninstall' ) );
        add_filter('plugin_action_links', array($this, 'demandware_add_settings_link'), 10, 2); // Future use for Upgrade of plugin                       
    }
    	
    private function update_version($version = '') {
    	
    	if($version == '') {
    		$version = DWO_VERSION;
    	}

    	update_option(DWO_OPTION_NAME, $version);
    }
    
    public function demandWareHook_activate() {
        
        global $wpdb;
        
        $table_name = $wpdb->prefix . 'demadware_log';
        
       // $charset_collate = $wpdb->get_charset_collate();
        
        $sql = "CREATE TABLE ". $table_name . " (
        log_id bigint(20) NOT NULL AUTO_INCREMENT,
        user_id bigint(20) NOT NULL,
        event varchar(60) NOT NULL,	
        post_id bigint(20) NOT NULL,
        server_id bigint(20) NOT NULL,
        status tinyint(1) NOT NULL DEFAULT 0,
        response longtext NOT NULL,
        log_time datetime NOT NULL,
        PRIMARY KEY  (log_id)
        );";
        
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        
        dbDelta( $sql );
               
        $this->update_version();
    }
    
    function demandWareHook_deactivate() {
    	// find out when the last event was scheduled
    	$timestamp = wp_next_scheduled ('audit_retention_log_event');
    	wp_unschedule_event ($timestamp, 'audit_retention_log_event');
    }
    
    public function demandware_add_settings_link($links, $file)
    {    
    	$demandWareFile = DWO_PLUGIN_FILE;    	 
        if (basename($file) == $demandWareFile) {
        	
            $linkSettings = '<a href="' . admin_url("edit.php?post_type=dw-servers") . '">Settings</a>';
            array_unshift($links, $linkSettings);
        }
        return $links;
    }
    
    public static function demandWareHook_uninstall() {
    	global $wpdb;
    	
    	$table_name = $wpdb->prefix . 'demadware_log';
    	
    	$wpdb->query( "DROP TABLE IF EXISTS {$table_name}" );
    }    
}

new demandWareHooks();

/*
 * CRON
 */

add_action('wp', 'cronstarter_activation');
function cronstarter_activation()
{	
	if( !wp_next_scheduled( 'audit_retention_log_event' ) ) {
		wp_schedule_event( time(), 'daily', 'audit_retention_log_event' );
	}
}

add_action('audit_retention_log_event','my_daily_function');
function my_daily_function() {	
	global $wpdb;
	$log_table = $wpdb->prefix . 'demadware_log';
	$type = 'dw-servers';
	$args=array(
			'post_type' => $type,
			'post_status' => 'publish',
			'posts_per_page' => -1
	);
	$server_query = null;
	$server_query = new WP_Query($args);
	if( $server_query->have_posts() ) {
		while ($server_query->have_posts()) : $server_query->the_post();
		$server_id = $server_query->post->ID;
		$auditLogDays = get_post_meta($server_id,"dw_audit_log",true);
		$currentDate = strtotime(date('Y-m-d H:i:s'));
		$endDate = strtotime('-'.$auditLogDays. 'day', $currentDate);
		$wpdb->query("DELETE FROM ". $log_table ." WHERE log_time < '".date("Y-m-d H:i:s",$endDate)."' and server_id = ".$server_id);
		endwhile;
	}	
}
