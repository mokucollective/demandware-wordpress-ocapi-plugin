<?php
/**
 * Plugin Name: Demandware OCAPI
 * URI: http://www.mokucollective.com/
 * Description: 
 * Author: Moku Collecive
 * Version: 1.0.1
 * Author URI: http://www.mokucollective.com/
 * Network: false
 * 
 */


	define ( 'DWO_VERSION', '1.0.1' );
	define ( 'DWO_DEBUG', TRUE );
	define ( 'DWO_PATH', plugin_dir_path ( __FILE__ ) );
	define ( 'DWO_URL', plugins_url ( '', __FILE__ ) );
	define ( 'DWO_PLUGIN_FILE', basename ( __FILE__ ) );
	define ( 'DWO_PLUGIN_DIR', plugin_basename ( dirname ( __FILE__ ) ) );
	define ( 'DWO_OPTION_NAME', 'DWO_settings' );
	define ( 'DWO_ADMIN_DIR', DWO_PATH . 'admin' );
	define ( 'DWO_ADMIN_WEB_URI', DWO_URL . '/admin' );
	define ( 'DWO_CLASS_DIR', 'class' );
	define ( 'DWO_CLASS', DWO_PATH . DWO_CLASS_DIR );	
	// Adding Hook Class
	
	require_once (DWO_CLASS . '/hook.php');
	require_once (DWO_ADMIN_DIR . '/class/menu.php');
	require_once (DWO_ADMIN_DIR . '/hook.php');
	require_once (DWO_ADMIN_DIR . '/assets.php');
	//require_once (DWO_CLASS . '/assets.php');
	//require_once (DWO_CLASS . '/show-view.php');
