=== Demandware OCAPI ===
Contributors: mokucollective
Tags: demandware, demandware api 
Requires at least: 3.5
Tested up to: 4.4.2
Stable tag: 1.0.1
License: GPLv2 or later

Integration with demandware server for pushing the post.

== Description ==

Integration with demandware server for pushing the post.

= Key Features =

- Control what to publish to Demandware; with support for automatic sync on Publish in WordPress.
- Single Article
- All Content
- Category Mappings
- Control where to publish the WordPress content
- Demandware clients maintains several server instances and it’s likely that a user would want to preview content in one server and publish it live in another. (eg: “Publish to QA”, “Publish to Production”)
- Configure the plugin
- Demandware environment setup
- Server List
- Other options
- Custom WordPress fields mapped to corresponding custom Demandware content fields


== Installation ==

1. Upload 'Demandware OCAPI' to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Done!

== Screenshots ==





== Changelog ==

= 1.0.0 =


* Initial release
